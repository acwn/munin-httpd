/*

Filename: main.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include <algorithm>
#include <iostream>
#include <string>
#include <array>
#include <exception>

#include "apache.hpp"
#include "certcheck.hpp"
#include "lighttpd.hpp"


int main (int argc, char *argv[])
{
  static const std::array<std::string,10> plugins_list {"lighttpd_servers", "lighttpd_average", "lighttpd_traffictotal", "lighttpd_trafficaverage", "lighttpd_uptime", "apache_servers", "apache_average", "apache_traffictotal", "apache_uptime", "certcheck"};
  static const std::array<std::string,2> actions_list {"config", "fetch"};
  static const std::string execname (basename (argv[0]));

  if (argc == 2)
  {
    std::string action (argv[1]);
    if (std::find (actions_list.begin (), actions_list.end (), action) != actions_list.end ())
    {
      if (execname == plugins_list[0])
      {
        Lighttpd (servers, action);
      }
      else if (execname == plugins_list[1])
      {
        Lighttpd (average, action);
      }
      else if (execname == plugins_list[2])
      {
        Lighttpd (traffictotal, action);
      }
      else if (execname == plugins_list[3])
      {
        Lighttpd (trafficaverage, action);
      }
      else if (execname == plugins_list[4])
      {
        Lighttpd (uptime, action);
      }
      else if (execname == plugins_list[5])
      {
        Apache (servers, action);
      }
      else if (execname == plugins_list[6])
      {
        Apache (average, action);
      }
      else if (execname == plugins_list[7])
      {
        Apache (traffictotal, action);
      }
      else if (execname == plugins_list[8])
      {
        Apache plugin (uptime, action);
      }
      else if (execname == plugins_list[9])
      {
        Certcheck plugin (action);
      }
      else
      {
        std::cout << "Unknown plugin!\n";
        return 0;
      }
    }
  }
  else
  {
    std::cout << "Not enough arguments!\n";
    std::cout << "Please read the documentation at: https://www.acwn.de/projects/munin-httpd \n";
  }
  return 0;
}
