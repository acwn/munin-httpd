/*

Filename: certcheck.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __CERTCHECK__
#define __CERTCHECK__

#include <map>
#include <shared_mutex>
#include <vector>

class Certcheck
{
  private:
    std::vector <std::string> m_domainlist;
    std::map <std::string, int> m_results;
    std::shared_mutex m_results_mutex;

    void ReadConfig (void);
    void PrintDomain (std::string domain);
  public:
    explicit Certcheck (const std::string &action);
    ~Certcheck ();
    void PrintConfig (void);
    void PrintFetch (void);
};

#endif
