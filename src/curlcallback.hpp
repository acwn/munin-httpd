/*

Filename: curlcallback.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __CURLCALLBACK__
#define __CURLCALLBACK__

#include <string>

struct s_CurlData
{
  std::string answer;
  std::size_t size;
};

class CurlCallback
{
  public:
    static std::size_t OnWrite (void  *ptr,  std::size_t  size, std::size_t nmemb, void *stream);
    static std::size_t OnRead  (void  *ptr,  std::size_t  size, std::size_t nmemb, void *stream);
};

#endif
