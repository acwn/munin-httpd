/*

Filename: lighttpd.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __LIGHTTPD__
#define __LIGHTTPD__

#include <string>

#define RAPIDJSON_SSE2
#include <rapidjson/rapidjson.h>

#include "plugin.hpp"

class Lighttpd
{
  private:
    void PrintConfig (e_typus typus);
    void PrintFetch (e_typus typus);
  public:
    Lighttpd (e_typus typus, const std::string &action);
};

#endif
