/*

Filename: apache.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __APACHE__
#define __APACHE__

#include "plugin.hpp"

class Apache
{
  private:
    void PrintConfig (e_typus typus);
    void PrintFetch (e_typus typus);
  public:
    Apache (e_typus typus, const std::string &action);
};

#endif
