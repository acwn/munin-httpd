/*

Filename: curl.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include "curl.hpp"

#include <map>
#include <utility>
#include <iostream>
#include <exception>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

Curl::Curl (void)
{
	curl_global_init (CURL_GLOBAL_ALL);
	hCurl = curl_easy_init ();

	SetParameter (CURLOPT_READFUNCTION, (void*)CurlCallback::OnRead);
	SetParameter (CURLOPT_READDATA, &sData);
	SetParameter (CURLOPT_WRITEFUNCTION, (void*) CurlCallback::OnWrite);
	SetParameter (CURLOPT_WRITEDATA, (void*) & sData);
	SetParameter (CURLOPT_SSL_VERIFYPEER, 0);
	SetParameter (CURLOPT_FOLLOWLOCATION, 1);
	SetParameter (CURLOPT_UNRESTRICTED_AUTH, 1);
	SetParameter (CURLOPT_CERTINFO, 1);
	SetParameter (CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	SetParameter (CURLOPT_PROTOCOLS, CURLPROTO_HTTP|CURLPROTO_HTTPS);
	SetParameter (CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP|CURLPROTO_HTTPS);
}

Curl::~Curl ()
{
	curl_easy_cleanup (hCurl);
	curl_global_cleanup ();
}

void Curl::SetUrl (std::string url)
{
  SetParameter (CURLOPT_URL, url.c_str ());
}

void Curl::SetUserAgent (std::string name, std::string version)
{
  std::string user_agent (name);
  user_agent.append ("/").append (version);
  SetParameter (CURLOPT_USERAGENT, user_agent.c_str ());
}

std::string Curl::GetLastError (CURLcode last_error)
{
  return curl_easy_strerror (last_error);
}

std::string Curl::GetContent (void)
{
  CURLcode last_error = curl_easy_perform (hCurl);
	if (last_error != CURLE_OK)
	{
		throw std::runtime_error (GetLastError (last_error));
	}
	return sData.answer;
}

std::string Curl::GetExpirationDate (std::string &domain)
{
  std::string data;
  struct curl_certinfo *certinfo;
  CURLcode res;
  
  res = curl_easy_getinfo(hCurl, CURLINFO_CERTINFO, &certinfo);
  if(res == CURLE_OK && certinfo)
  {
    for (int i = 0; i < certinfo->num_of_certs; i++)
    {
      struct curl_slist *slist;
      for (slist = certinfo->certinfo[i]; slist; slist = slist->next)
      {
        data = slist->data;
/* We assume the first certificate ist the correct one. */
        if (data.find ("Expire Date") != std::string::npos)
        {
          return data.substr (data.find_first_of (":") + 1, data.length());
        }
      }
    }
  }
  return NULL;
}
