/*

Filename: curl.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __CURL__
#define __CURL__

#include <stdexcept>
#include <string>
#include <curl/curl.h>

#include "curlcallback.hpp"

class Curl
{
  public:
    Curl (void);
    ~Curl ();
    void SetUrl (std::string url);
    void SetUserAgent (std::string name, std::string version);
    std::string GetContent (void);
    std::string GetExpirationDate (std::string &domain);
    template <typename T> void SetParameter (CURLoption option, T parameter)
    {
      CURLcode last_error = curl_easy_setopt (hCurl, option, parameter);
      if (last_error != CURLE_OK)
      {
         throw std::runtime_error (GetLastError (last_error));
      }
    }
  private:
    friend class CurlCallback;
    CURL *hCurl;
    struct s_CurlData sData;

    std::string GetLastError (CURLcode last_error);
};

#endif
