/*

Filename: certcheck.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <memory>
#include <sstream>
#include <thread>

#include <yaml-cpp/yaml.h>

#include "certcheck.hpp"
#include "curl.hpp"

// #ifdef HAVE_CONFIG_H
#include <config.h>
// #endif

Certcheck::Certcheck (const std::string &action)
{
  ReadConfig ();
  if (action == "config")
    PrintConfig ();
  else if (action == "fetch")
    PrintFetch ();
}

Certcheck::~Certcheck ()
{
}

void Certcheck::PrintConfig (void)
{
  std::string fieldname;
  std::cout << "graph_args --base 1000 --lower-limit 0\n";
  std::cout << "graph_category Certificates\n";
  std::cout << "graph_title Expiration\n";
  std::cout << "graph_vlabel days\n";

  for (std::string d : m_domainlist)
  {
    fieldname = d;
    std::replace (fieldname.begin(), fieldname.end(), '.','_');
    std::cout << fieldname << ".label " << d << std::endl;
    std::cout << fieldname << ".draw LINE\n";
    std::cout << fieldname << ".info Expiration in days for " << d << std::endl;
    std::cout << fieldname << ".warning 10:\n";
    std::cout << fieldname << ".critical 5:\n";
  }
}

void Certcheck::PrintFetch (void)
{
  std::vector <std::thread> threadlist;

  for (const std::string& d : m_domainlist)
  {
    threadlist.push_back (std::thread (&Certcheck::PrintDomain, this, d));
  }

  for (std::thread& th : threadlist)
  {
    th.join ();
  }

  for (const auto &pair : m_results)
  {
    std::cout << pair.first << ".value " << pair.second << std::endl;
  }
}

void Certcheck::ReadConfig (void)
{
  YAML::Node config = YAML::LoadFile ("/etc/munin/plugin-conf.d/munin-httpd.yaml");

  for (YAML::const_iterator it=config["domains"].begin(); it != config["domains"].end (); ++it)
  {
    m_domainlist.push_back (it->as<std::string>());
  }
}

void Certcheck::PrintDomain (std::string domain)
{
  try
  {
    std::string fieldname;
    std::tm t = {};
    fieldname = domain;
    std::replace (fieldname.begin(), fieldname.end(), '.','_');
    std::unique_ptr <Curl> curly (new Curl ());
    curly->SetUserAgent (PACKAGE_NAME, PACKAGE_VERSION);
    curly->SetUrl ("https://" + domain + "/");
    curly->SetParameter (CURLOPT_NOBODY, 1L);
    curly->GetContent ();
    std::istringstream ss (curly->GetExpirationDate (domain));
    ss >> std::get_time (&t, "%Y-%m-%d %H:%M:%S");

    if (!ss.fail ())
    {
      int certdate = std::mktime(&t);
      std::time_t lt = std::time (nullptr);
      std::tm localdate = *std::localtime(&lt);
      int date = std::mktime (&localdate);
      std::lock_guard<std::shared_mutex> guard (m_results_mutex);
      m_results [fieldname] = (certdate - date)/86400;
    }
    else
    {
      std::cout << "Parse failed" << std::endl;
    }
  }
  catch (std::exception &e)
  {
    std::cout << e.what() << std::endl;
  }
}
