/*

Filename: plugin.hpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#ifndef __PLUGIN__
#define __PLUGIN__

enum e_typus
{
  servers,
  average,
  traffictotal,
  trafficaverage,
  uptime
};

#endif
