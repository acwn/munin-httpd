/*

Filename: apache.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include <regex>
#include <iostream>

#include "apache.hpp"
#include "curl.hpp"

// #ifdef HAVE_CONFIG_H
#include <config.h>
// #endif

Apache::Apache (e_typus typus, const std::string &action)
{
  if (action == "config")
    PrintConfig (typus);
  else if (action == "fetch")
    PrintFetch (typus);
}

void Apache::PrintConfig (e_typus typus)
{
  std::cout << "graph_args --base 1000 --lower-limit 0" << std::endl;
  std::cout << "graph_category Apache" << std::endl;

  switch (typus)
  {
    case servers:
      std::cout << "graph_title Apache workers" << std::endl;
      std::cout << "graph_vlabel Workers" << std::endl;
      std::cout << "busyworkers.label busy servers" << std::endl;
      std::cout << "idleworkers.label idle servers" << std::endl;
      std::cout << "busyworkers.draw LINE1" << std::endl;
      std::cout << "idleworkers.draw LINE2" << std::endl;
      std::cout << "busyworkers.info busy servers" << std::endl;
      std::cout << "idleworkers.info idle servers" << std::endl;
    break;
    case average:
      std::cout << "graph_title Apache Average" << std::endl;
      std::cout << "graph_vlabel Average" << std::endl;
      std::cout << "requestspersecond.label Requests per second" << std::endl;
      std::cout << "bytespersecond.label Bytes per second" << std::endl;
      std::cout << "durationperreq.label Duration per request" << std::endl;
      std::cout << "bytesperreq.label Bytes per request" << std::endl;
      std::cout << "requestspersecond.draw LINE1" << std::endl;
      std::cout << "bytespersecond.draw LINE2" << std::endl;
      std::cout << "durationperreq.draw LINE3" << std::endl;
      std::cout << "bytesperreq.draw LINE4" << std::endl;
      std::cout << "requestspersecond.info Requests per second" << std::endl;
      std::cout << "bytespersecond.info Bytes per second" << std::endl;
      std::cout << "durationperreq.info Duration per request" << std::endl;
      std::cout << "bytesperreq.info Bytes per request" << std::endl;
    break;
    case traffictotal:
      std::cout << "graph_title Apache Total" << std::endl;
      std::cout << "graph_vlabel Total" << std::endl;
      std::cout << "totalaccesses.label Total Accesses" << std::endl;
      std::cout << "totalkbytes.label Total kBytes" << std::endl;
      std::cout << "totalaccesses.draw LINE1" << std::endl;
      std::cout << "totalkbytes.draw LINE2" << std::endl;
      std::cout << "totalaccesses.info Total Accesses" << std::endl;
      std::cout << "totalkbytes.info Total kBytes" << std::endl;
    break;
    case uptime:
      std::cout << "graph_title Apache uptime" << std::endl;
      std::cout << "graph_vlabel Uptime in days" << std::endl;
      std::cout << "uptime.label Uptime" << std::endl;
      std::cout << "uptime.draw AREA" << std::endl;
      std::cout << "uptime.info Uptime" << std::endl;
    break;
    default:
    break;
  }
}

void Apache::PrintFetch (e_typus typus)
{
  std::string content;
  /* regex */
  std::regex re_busy ("BusyWorkers: ([0-9]{1,9})");
  std::regex re_idle ("IdleWorkers: ([0-9]{1,9})");

  std::regex re_reqpersec ("ReqPerSec: ([0-9.]{1,9})");
  std::regex re_bytespersec ("BytesPerSec: ([0-9.]{1,9})");
  std::regex re_durationperreq ("DurationPerReq: ([0-9.]{1,9})");
  std::regex re_bytesperreq ("BytesPerReq: ([0-9.]{1,9})");

  std::regex re_total_accesses ("Total Accesses: ([0-9]{1,9})");
  std::regex re_total_kbytes ("Total kBytes: ([0-9]{1,9})");

  std::regex re_total ("ServerUptimeSeconds: ([0-9]{1,9}) ");
  std::regex re_uptime ("ServerUptimeSeconds: ([0-9]{1,9})");
  std::smatch match;

  try
  {
    std::unique_ptr <Curl> curly (new Curl ());
    curly->SetUserAgent (PACKAGE_NAME, PACKAGE_VERSION);
    curly->SetUrl ("http://127.0.0.1/server-status?auto");
    content = curly->GetContent ();
    switch (typus)
    {
      case servers:
        if (std::regex_search (content, match, re_busy))
        {
          std::cout << "busyworkers.value " << match[1].str() << std::endl;
        }
        if (std::regex_search (content, match, re_idle))
        {
          std::cout << "idleworkers.value " << match[1].str() << std::endl;
        }
      break;
      case average:
        if (std::regex_search (content, match, re_reqpersec))
        {
          std::cout << "requestspersecond.value " << match[1].str() << std::endl;
        }
        if (std::regex_search (content, match, re_bytespersec))
        {
          std::cout << "bytespersecond.value " << match[1].str() << std::endl;
        }
        if (std::regex_search (content, match, re_durationperreq))
        {
          std::cout << "durationperreq.value " << match[1].str() << std::endl;
        }
        if (std::regex_search (content, match, re_bytesperreq))
        {
          std::cout << "bytesperreq.value " << match[1].str() << std::endl;
        }
      break;
      case traffictotal:
        if (std::regex_search (content, match, re_total_accesses))
        {
          std::cout << "totalaccesses.value " << match[1].str() << std::endl;
        }
        if (std::regex_search (content, match, re_total_kbytes))
        {
          std::cout << "totalkbytes.value " << match[1].str() << std::endl;
        }
      break;
      case uptime:
        if (std::regex_search (content, match, re_uptime))
        {
          std::cout << "uptime.value " << match[1].str() << std::endl;
        }
      break;
      default:
      break;
    }
  }
  catch (std::exception &e)
  {
    std::cout << e.what() << std::endl;
  }
}
