/*

Filename: lighttpd.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include <iostream>
#include <memory>
#include <rapidjson/document.h>

#include "lighttpd.hpp"
#include "curl.hpp"
#include "config.h"

Lighttpd::Lighttpd (e_typus typus, const std::string &action)
{
  if (action == "config")
    PrintConfig (typus);
  else if (action == "fetch")
    PrintFetch (typus);
}

void Lighttpd::PrintConfig (e_typus typus)
{
  std::cout << "graph_args --base 1000 --lower-limit 0\n";
  std::cout << "graph_category Lighttpd\n";

  switch (typus)
  {
    case servers:
      std::cout << "graph_title Lighttpd servers\n";
      std::cout << "graph_vlabel Servers\n";
      std::cout << "busyservers.label busy servers\n";
      std::cout << "idleservers.label idle servers\n";
      std::cout << "busyservers.draw LINE1\n";
      std::cout << "idleservers.draw LINE2\n";
      std::cout << "busyservers.info busy servers\n";
      std::cout << "idleservers.info idle servers\n";
    break;
    case average:
      std::cout << "graph_title Lighttpd Average\n";
      std::cout << "graph_vlabel Average\n";
      std::cout << "requestaverage5s.label Requests (average over 5s)\n";
      std::cout << "trafficaverage5s.label Traffic (average over 5s)\n";
      std::cout << "requestaverage5s.draw LINE1\n";
      std::cout << "trafficaverage5s.draw LINE2\n";
      std::cout << "requestaverage5s.info Requests (average over 5s)\n";
      std::cout << "trafficaverage5s.info Traffic (average over 5s)\n";
    break;
    case traffictotal:
      std::cout << "graph_title Lighttpd Traffic Total\n";
      std::cout << "graph_vlabel Total\n";
      std::cout << "requeststotal.label Requests total\n";
      std::cout << "traffictotal.label Traffic total\n";
      std::cout << "requeststotal.draw LINE1\n";
      std::cout << "traffictotal.draw LINE2\n";
      std::cout << "requeststotal.info Requests total\n";
      std::cout << "traffictotal.info Traffic total\n";
    break;
    case trafficaverage:
      std::cout << "graph_title Lighttpd Traffic Average\n";
      std::cout << "graph_vlabel Average\n";
      std::cout << "requestsaverage.label Requests/s\n";
      std::cout << "trafficaverage.label Traffic kb/s\n";
      std::cout << "requestsaverage.draw LINE1\n";
      std::cout << "trafficaverage.draw LINE2\n";
      std::cout << "requestsaverage.info Requests per second\n";
      std::cout << "trafficaverage.info Traffic kb per second\n";
    break;
    case uptime:
      std::cout << "graph_title Lighttpd uptime\n";
      std::cout << "graph_vlabel Uptime in days\n";
      std::cout << "uptime.label Uptime\n";
      std::cout << "uptime.draw AREA\n";
      std::cout << "uptime.info Uptime\n";
    break;
  }
}

void Lighttpd::PrintFetch (e_typus typus)
{
  std::string content;
  rapidjson::Document document;

  try
  {
    std::unique_ptr <Curl> curly (new Curl ());
    curly->SetUserAgent (PACKAGE_NAME, PACKAGE_VERSION);
    curly->SetUrl ("http://127.0.0.1/server-status?json");
    content = curly->GetContent ();
    document.Parse (content.c_str());
    switch (typus)
    {
      case servers:
        std::cout << "busyservers.value " << document["BusyServers"].GetInt () << "\n";
        std::cout << "idleservers.value " << document["IdleServers"].GetInt () << "\n";
      break;
      case average:
        std::cout << "requestaverage5s.value " << document["RequestAverage5s"].GetInt () << "\n";
        std::cout << "trafficaverage5s.value " << document["TrafficAverage5s"].GetInt () << "\n";
      break;
      case traffictotal:
        std::cout << "requeststotal.value " << document["RequestsTotal"].GetInt () << "\n";
        std::cout << "traffictotal.value " << document["TrafficTotal"].GetInt () << "\n";
      break;
      case trafficaverage:
        std::cout << "requestsaverage.value " << document["RequestsTotal"].GetFloat () / document["Uptime"].GetFloat () << "\n";
        std::cout << "trafficaverage.value " <<  document["TrafficTotal"].GetFloat () / document["Uptime"].GetFloat () << "\n";
      break;
      case uptime:
        std::cout << "uptime.value " << document["Uptime"].GetFloat ()/86400 << "\n";
      break;
    }
  }
  catch (std::exception &e)
  {
    std::cout << e.what() << "\n";
  }
}
