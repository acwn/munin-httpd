/*

Filename: curlcallback.cpp
Copyright © 2020 - 2021  Alexander Nagel
Email: munin-httpd@acwn.de
Homepage: http://www.acwn.de/projects/munin-httpd

This file is part of munin-httpd.

*/

#include "curlcallback.hpp"

std::size_t CurlCallback::OnWrite (void  *ptr,  std::size_t  size, std::size_t nmemb, void *stream)
{
  std::size_t f_size = size * nmemb;
  struct s_CurlData* mem = (struct s_CurlData*) stream;

  mem->answer.append ((char*) ptr, f_size);
  return f_size;
}

std::size_t CurlCallback::OnRead (void  *ptr,  std::size_t  size, std::size_t nmemb, void *stream)
{
  std::size_t f_size = 0;

  return f_size;
}
