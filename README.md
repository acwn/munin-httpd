# munin-httpd

A munin plugin that monitors the status of lighttpd or apache web server.


## What you need

* libcurl
* rapidjson
* libyaml-cpp

## How to compile

    meson setup build
    meson compile -C build

## How to install

Copy the binary (./build/munin-httpd) in the munin plugin folder.

Create symlinks from /etc/munin/plugins to the application like this:
    ln -s /usr/share/munin/plugins/munin-httpd lighttpd_average

These are the possible symlink names:

* lighttpd_servers

* lighttpd_average

* lighttpd_total

* lighttpd_uptime

* apache_servers

* apache_average

* apache_total

* apache_uptime

* certcheck

This check needs a config file /etc/munin/plugin-conf.d/munin-httpd.yaml
List your domains here like in the example.
