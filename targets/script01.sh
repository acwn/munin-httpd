#!/bin/sh
# Do a static code check
cppcheck --error-exitcode=1 --enable=all --suppress=useStlAlgorithm ./../src/*.cpp
